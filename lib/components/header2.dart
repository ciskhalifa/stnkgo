import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';

import '../enums/constants.dart';

class Header2 extends StatelessWidget {
  const Header2({
    Key key,
    @required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: kDefaultPadding),
      height: size.height * 0.2,
      child: Stack(
        children: <Widget>[
          Container(
            height: size.height * 0.2 - 27,
            decoration: BoxDecoration(
                color: kPrimaryColor,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(36),
                  bottomRight: Radius.circular(36),
                )),
          ),
          Positioned(
            left: 0,
            right: 0,
            child: SvgPicture.asset(
              "assets/images/logo2.svg",
              height: 90.0,
              width: 100.0,
              allowDrawingOutsideViewBox: false,
              alignment: Alignment.center,
            ),
          ),
        ],
      ),
    );
  }
}
