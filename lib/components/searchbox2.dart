import 'package:flutter/material.dart';
import 'package:stnkgo/screens/tracking/components/tracking.dart';
import 'package:stnkgo/screens/tracking/components/tracking2.dart';

import '../enums/constants.dart';

class SearchBox2 extends StatefulWidget {
  @override
  _SearchBox2State createState() => _SearchBox2State();
}

class _SearchBox2State extends State<SearchBox2> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController inputController = new TextEditingController();
  String textInput;
  final snackBar = SnackBar(
    content: Text('No. Resi wajib di isi.'),
    action: SnackBarAction(
      label: 'Undo',
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: kDefaultPadding),
            child: Text(
              "Tracking Sistem",
              style: TextStyle(
                  shadows: [
                    Shadow(
                      blurRadius: 10.0,
                      color: Colors.black.withOpacity(0.3),
                      offset: Offset(3.0, 3.0),
                    ),
                  ],
                  color: Colors.black,
                  fontFamily: "Futura",
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
            padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: kPrimaryColor.withOpacity(0.23)),
              ],
            ),
            child: Row(
              children: <Widget>[
                Form(
                  key: _formKey,
                  child: Expanded(
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      controller: inputController,
                      onFieldSubmitted: (String text) {
                        if (_formKey.currentState.validate()) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Tracking(title: text)),
                          );
                        }
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return "No. Resi tidak boleh kosong";
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        icon: Icon(Icons.search),
                        hintText: "No.Resi",
                        hintStyle: TextStyle(
                          color: kPrimaryColor.withOpacity(0.5),
                        ),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
