import 'package:flutter/material.dart';
import 'package:stnkgo/screens/tracking/components/tracking.dart';

import '../enums/constants.dart';

class SearchBox extends StatefulWidget {
  @override
  _SearchBoxState createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController inputController = new TextEditingController();
  String textInput;
  final snackBar = SnackBar(
    content: Text('No. Resi wajib di isi.'),
    action: SnackBarAction(
      label: 'Undo',
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
      padding: EdgeInsets.symmetric(horizontal: kDefaultPadding),
      height: 50,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
              offset: Offset(0, 10),
              blurRadius: 50,
              color: kPrimaryColor.withOpacity(0.23)),
        ],
      ),
      child: Row(
        children: <Widget>[
          Form(
            key: _formKey,
            child: Expanded(
              child: TextFormField(
                keyboardType: TextInputType.number,
                controller: inputController,
                onFieldSubmitted: (String text) {
                  if (_formKey.currentState.validate()) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Tracking(title: text)),
                    );
                  }
                },
                validator: (value) {
                  if (value.isEmpty) {
                    return "No. Resi tidak boleh kosong";
                  } else {
                    return null;
                  }
                },
                decoration: InputDecoration(
                  icon: Icon(Icons.search),
                  hintText: "No.Resi",
                  hintStyle: TextStyle(
                    color: kPrimaryColor.withOpacity(0.5),
                  ),
                  enabledBorder: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
