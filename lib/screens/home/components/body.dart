import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:stnkgo/components/searchbox2.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:stnkgo/components/searchbox.dart';
import 'package:stnkgo/enums/constants.dart';

import '../../../components/header_with_searchbox.dart';

class Body extends StatelessWidget {
  DateTime currentBackPressTime;
  final List<String> imgList = [
    'https://stnkgo.com/assets/mobile/home/APP1.jpg',
    'https://stnkgo.com/assets/mobile/home/APP2.jpg',
    'https://stnkgo.com/assets/mobile/home/APP3.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                HeaderWithSearchBox(size: size),
                SearchBox2(),
                Container(
                  child: Column(
                    children: <Widget>[
                      CarouselSlider(
                        options: CarouselOptions(
                          height: size.height / 3,
                          autoPlay: true,
                          enlargeCenterPage: true,
                          autoPlayInterval: Duration(seconds: 5),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 1000),
                        ),
                        items: imgList.map((i) {
                          return Builder(
                            builder: (BuildContext context) {
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                margin: EdgeInsets.symmetric(
                                    vertical: kDefaultPadding * 2),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadiusDirectional.circular(10),
                                    color: kPrimaryColor),
                                child: Stack(
                                  children: <Widget>[
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Image.network(
                                          i,
                                          loadingBuilder: (BuildContext context,
                                              Widget child,
                                              ImageChunkEvent loadingProgress) {
                                        if (loadingProgress == null)
                                          return child;
                                        return Center(
                                          child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                    Color>(Colors.white),
                                            value: loadingProgress
                                                        .expectedTotalBytes !=
                                                    null
                                                ? loadingProgress
                                                        .cumulativeBytesLoaded /
                                                    loadingProgress
                                                        .expectedTotalBytes
                                                : null,
                                          ),
                                        );
                                      },
                                          fit: BoxFit.fill,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              0.2,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              2),
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        onWillPop: onWillPop);
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();

    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      Fluttertoast.showToast(msg: "Click back button twice to exit from apps");
      return Future.value(false);
    }
    return Future.value(true);
  }
}
