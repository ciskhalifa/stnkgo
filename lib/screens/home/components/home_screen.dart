import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:stnkgo/enums/constants.dart';
import 'package:stnkgo/screens/home/components/body.dart';
import 'package:stnkgo/screens/layanan/components/layanan_screen2.dart';
import 'package:stnkgo/ui/base_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _listPage = <Widget>[
    Body(),
    LayananScreen2(),
  ];

  int _selectedIndex = 0;

  void _onNavBarTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _bottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.shippingFast),
        title: new Text(
          'TRACKING',
          style: TextStyle(
            fontFamily: 'Futura',
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
      BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.listAlt),
        title: Text(
          'ORDER',
          style: TextStyle(
            fontFamily: 'Futura',
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
    ];
    return BaseWidget(
      builder: (context, sizingInformation) {
        return Scaffold(
          body: _listPage[_selectedIndex],
          bottomNavigationBar: BottomNavigationBar(
            selectedItemColor: kPrimaryColor,
            unselectedItemColor: Colors.black,
            currentIndex:
                _selectedIndex, // this will be set when a new tab is tapped
            items: _bottomNavBarItems,
            onTap: _onNavBarTapped,
          ),
        );
      },
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      elevation: 0,
    );
  }
}
