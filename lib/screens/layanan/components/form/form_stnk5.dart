import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';

class FormStnk5 extends StatelessWidget {
  const FormStnk5({
    Key key,
    @required GlobalKey<FormBuilderState> fbKey,
    @required TextEditingController namaController,
    @required TextEditingController nopolController,
    @required TextEditingController hpController,
    @required String jenis,
    // ignore: non_constant_identifier_names
    @required String kode_layanan,
    @required onChanged,
  })  : _kode_layanan = kode_layanan,
        _jenis = jenis,
        _fbKey = fbKey,
        _namaController = namaController,
        _nopolController = nopolController,
        _hpController = hpController,
        super(key: key);

  final GlobalKey<FormBuilderState> _fbKey;
  final TextEditingController _namaController;
  final TextEditingController _nopolController;
  final TextEditingController _hpController;
  final String _jenis;
  // ignore: non_constant_identifier_names
  final String _kode_layanan;

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: _fbKey,
      initialValue: {
        'date': DateTime.now(),
        'jenis': _jenis,
        'kode_layanan': _kode_layanan,
      },
      autovalidateMode: AutovalidateMode.always,
      child: Column(
        children: <Widget>[
          CardRadio(
            attr: "jenis_kendaraan",
            judul: "Data Kebutuhan",
            label: "Jenis Kendaraan",
            radio1: "Motor",
            value1: '1',
            radio2: "Mobil",
            value2: '2',
          ),
          CardRadio(
            attr: "konfirmasi_stnk",
            judul: "Konfirmasi Data STNK",
            label: "Apakah data di STNK sama dengan data di KTP ?",
            radio1: "Ya",
            value1: '1',
            radio2: "Tidak",
            value2: '0',
          ),
          CardKendaraan(
            namaController: _namaController,
            nopolController: _nopolController,
            hpController: _hpController,
          ),
          // CardDokumentasi(onChanged: _onChanged),
          CardSyarat(
            deskripsi:
                "STNK GO akan segera mengambil dokumen Kendaraan Anda untuk kebutuhan proses administrasi pengurusan pajak kendaraan. \nMohon persiapkan dokumen beriku ini: \n\n1. KTP Asli Pemilik Kendaraan \n2. BPKB Asli atau Surat Keterangan Leasing \n3. STNK asli kendaraan \n4. Cek fisik kendaraan",
          ),
        ],
      ),
    );
  }
}

class CardSyarat extends StatelessWidget {
  final String deskripsi;

  const CardSyarat({
    Key key,
    this.deskripsi,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
              top: 10,
            ),
            child: Text(
              "Dokumen yang Harus Disiapkan",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Divider(
            color: Colors.grey[400],
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
              top: 10,
            ),
            child: Text(
              deskripsi,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CardDokumentasi extends StatelessWidget {
  const CardDokumentasi({
    Key key,
    @required onChanged,
  })  : _onChanged = onChanged,
        super(key: key);

  final _onChanged;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
              top: 10,
            ),
            child: Text(
              "Dokumentasi Data",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Divider(
            color: Colors.grey[400],
          ),
          LabelForm(
            label: "Foto KTP",
          ),
          CardUpload(
            onChanged: _onChanged,
            attr: 'ktp',
          ),
          LabelForm(
            label: "Foto SKPD",
          ),
          CardUpload(
            onChanged: _onChanged,
            attr: 'skpd',
          ),
          LabelForm(
            label: "Foto STNK",
          ),
          CardUpload(
            onChanged: _onChanged,
            attr: 'stnk',
          ),
        ],
      ),
    );
  }
}

class LabelForm extends StatelessWidget {
  final String label;
  const LabelForm({
    Key key,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(
        left: 15,
      ),
      child: Text(
        label,
        textAlign: TextAlign.start,
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}

class CardUpload extends StatelessWidget {
  final _onChanged;
  final String attr;

  const CardUpload({
    Key key,
    @required onChanged,
    this.attr,
  })  : _onChanged = onChanged,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.only(
        left: 15,
        bottom: 10,
        top: 10,
      ),
      child: FormBuilderImagePicker(
        attribute: attr,
        maxImages: 1,
        iconColor: Colors.red,
        // readOnly: true,
        validators: [
          FormBuilderValidators.required(),
        ],
        onChanged: _onChanged,
      ),
    );
  }
}

class CardKendaraan extends StatelessWidget {
  const CardKendaraan({
    Key key,
    @required TextEditingController namaController,
    @required TextEditingController nopolController,
    @required TextEditingController hpController,
  })  : _namaController = namaController,
        _nopolController = nopolController,
        _hpController = hpController,
        super(key: key);

  final TextEditingController _namaController;
  final TextEditingController _nopolController;
  final TextEditingController _hpController;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              top: 10,
            ),
            child: Text(
              "Nama Lengkap",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
            ),
            child: FormBuilderTextField(
              attribute: 'nama',
              // autovalidate: true,
              controller: _namaController,
              decoration: InputDecoration(
                hintText: "Nama Lengkap",
                labelText: 'Nama Lengkap',
              ),
              validators: [
                FormBuilderValidators.required(),
              ],
              keyboardType: TextInputType.text,
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              top: 10,
            ),
            child: Text(
              "Nomor Polisi Kendaraan",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
            ),
            child: FormBuilderTextField(
              attribute: 'nopol',
              // autovalidate: true,
              controller: _nopolController,
              decoration: InputDecoration(
                hintText: "D 1234 ABC",
                labelText: 'Nomor Polisi',
              ),
              validators: [
                FormBuilderValidators.required(),
              ],
              keyboardType: TextInputType.text,
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
            ),
            child: Text(
              "Tanggal Terakhir Pembayaran PKB",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
            ),
            child: FormBuilderDateTimePicker(
              validators: [
                FormBuilderValidators.required(),
              ],
              attribute: "date",
              inputType: InputType.date,
              format: DateFormat("yyyy-MM-dd"),
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
            ),
            child: Text(
              "No. Telpon / Whatsapp",
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
            ),
            child: FormBuilderTextField(
              attribute: 'hp',
              // autovalidate: true,
              controller: _hpController,
              decoration: InputDecoration(
                hintText: "621122334455",
                labelText: 'No. Telpon / Whatsapp',
              ),
              validators: [
                FormBuilderValidators.required(),
              ],
              keyboardType: TextInputType.number,
            ),
          ),
        ],
      ),
    );
  }
}

class CardRadio extends StatelessWidget {
  final String judul, label, radio1, radio2, value1, value2, attr;
  const CardRadio({
    Key key,
    this.judul,
    this.label,
    this.radio1,
    this.radio2,
    this.value1,
    this.value2,
    this.attr,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
              bottom: 10,
              top: 10,
            ),
            child: Text(
              judul,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Divider(
            color: Colors.grey[400],
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
            ),
            child: Text(
              label,
              textAlign: TextAlign.start,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            padding: EdgeInsets.only(
              left: 15,
            ),
            child: FormBuilderChoiceChip(
              decoration: InputDecoration(
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
              ),
              spacing: 30,
              attribute: attr,
              validators: [
                FormBuilderValidators.required(),
              ],
              options: [
                FormBuilderFieldOption(
                  child: Text(
                    radio1,
                  ),
                  value: value1,
                ),
                FormBuilderFieldOption(
                  child: Text(
                    radio2,
                  ),
                  value: value2,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
