class Layanan {
  final String image, value, title, heading, deskripsi, layanan, jenis;
  final int id;

  Layanan({
    this.id,
    this.image,
    this.value,
    this.title,
    this.heading,
    this.deskripsi,
    this.layanan,
    this.jenis,
  });
}

List<Layanan> layananx = [
  Layanan(
    id: 1,
    layanan: '1',
    title: "Perpanjangan Pajak Tahunan",
    jenis: "PPT",
    image: "https://stnkgo.com/assets/mobile/layanan/APP1.jpg",
    value: "",
    heading: "Pajak STNK mau habis? Tak risau lagi!",
    deskripsi:
        "STNK GO menyediakan layanan Perpanjangan Pajak tahunan. \n\nProses lebih mudah menggunakan pelayanan dari STNK GO \n\nSTNK GO siap antar jemput dokumen kendaraan anda",
  ),
  Layanan(
    id: 2,
    layanan: '2',
    title: "Perpanjangan Pajak 5 Tahunan",
    jenis: "PP5T",
    image: "https://stnkgo.com/assets/mobile/layanan/APP2.jpg",
    value: "",
    heading: "Tak usah ribet dengan nomor rangka dan mesin, kami urusin!",
    deskripsi:
        "STNK GO menyediakan layanan Perpanjangan Pajak 5 tahunan. \n\nProses lebih mudah menggunakan pelayanan dari STNK GO \n\nSTNK GO siap antar jemput dokumen kendaraan anda",
  ),
  Layanan(
    id: 3,
    layanan: '3',
    title: "Mutasi Kendaraan",
    jenis: "MK",
    image: "https://stnkgo.com/assets/mobile/layanan/APP3.jpg",
    value: "",
    heading: "Mutasi Kendaraan itu ribet! Percayakan pada kami.",
    deskripsi:
        "STNK GO menyediakan layanan Mutasi Kendaraan. \n\nProses lebih mudah menggunakan pelayanan dari STNK GO \n\nSTNK GO siap antar jemput dokumen kendaraan anda",
  ),
  Layanan(
    id: 4,
    layanan: '4',
    title: "Balik Nama Kendaraan (BBN)",
    jenis: "BBN",
    image: "https://stnkgo.com/assets/mobile/layanan/APP4.jpg",
    value: "",
    heading: "Balik Nama itu ribet! Percayakan hanya kepada kami.",
    deskripsi:
        "STNK GO menyediakan layanan Balik Nama Kendaraan. \n\nProses lebih mudah menggunakan pelayanan dari STNK GO \n\nSTNK GO siap antar jemput dokumen kendaraan anda",
  ),
];
