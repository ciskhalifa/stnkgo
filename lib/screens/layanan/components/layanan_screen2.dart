import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:stnkgo/enums/constants.dart';

import 'detail_layanan.dart';
import 'layanan.dart';

class LayananScreen2 extends StatefulWidget {
  final Layanan layanan;
  const LayananScreen2({Key key, this.layanan}) : super(key: key);
  @override
  _LayananScreen2State createState() => _LayananScreen2State();
}

class _LayananScreen2State extends State<LayananScreen2> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: size.height * 0.2,
            child: Center(
              child: SvgPicture.asset(
                "assets/images/logo2.svg",
                height: 90.0,
                width: 100.0,
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
              ),
            ),
            decoration: BoxDecoration(
              color: kPrimaryColor,
            ),
          ),
          Expanded(
            child: GridView.builder(
              itemCount: layananx.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: kDefaultPadding,
                crossAxisSpacing: kDefaultPadding / 2,
                crossAxisCount: 2,
                childAspectRatio: 0.75,
              ),
              itemBuilder: (context, index) => ItemCard(
                press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailLayanan(
                      layanan: layananx[index],
                    ),
                  ),
                ),
                layanan: layananx[index],
              ),
            ),
          ),
        ],
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back_arrow.svg",
            color: kPrimaryColor),
        onPressed: () => Navigator.pop(context),
      ),
      centerTitle: true,
      title: Text(
        "Layanan Kami".toUpperCase(),
        style: TextStyle(
          color: kPrimaryColor,
          fontFamily: 'Futura',
          fontSize: 20.0,
        ),
      ),
    );
  }
}

class ItemCard extends StatelessWidget {
  final Layanan layanan;
  final Function press;

  const ItemCard({
    Key key,
    this.layanan,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: kDefaultPadding / 2),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 6,
                offset: Offset(1, 1),
              ),
            ],
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 100,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      image: DecorationImage(
                        image: NetworkImage(layanan.image),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.all(5),
                child: Text(
                  layanan.title,
                ),
              ),
              Column(
                children: <Widget>[
                  Hero(
                    tag: layanan.id,
                    child: SizedBox(
                      width: 100,
                      child: RaisedButton(
                        child: Text(
                          "Detail",
                          style: TextStyle(color: kPrimaryColor, fontSize: 14),
                        ),
                        onPressed: press,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: kPrimaryColor),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
