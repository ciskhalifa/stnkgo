class Cabang {
  final int kode;
  final String value;

  Cabang({
    this.kode,
    this.value,
  });
}

List<Cabang> cabang = [
  Cabang(
    kode: 1,
    value: 'RIAU',
  ),
  Cabang(
    kode: 2,
    value: 'ARIA JIPANG',
  ),
  Cabang(
    kode: 3,
    value: 'SUBANG',
  ),
  Cabang(
    kode: 4,
    value: 'BEKASI',
  ),
  Cabang(
    kode: 5,
    value: 'ALKATERI',
  ),
];
