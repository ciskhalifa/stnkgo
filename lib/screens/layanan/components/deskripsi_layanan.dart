import 'package:flutter/material.dart';
import 'package:stnkgo/enums/constants.dart';

import 'package:stnkgo/screens/layanan/components/layanan.dart';

import 'form_layanan.dart';

class DeskripsiLayanan extends StatelessWidget {
  final Layanan layanan;

  const DeskripsiLayanan({Key key, this.layanan}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: size.width,
              height: size.height - 350,
              child: Stack(
                children: <Widget>[
                  Image.network(
                    layanan.image,
                    width: double.infinity,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: size.height * 0.33),
                    padding: EdgeInsets.only(
                      top: size.height * 0.01,
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                    ),
                    // height: 500,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Text(
                      layanan.title,
                      style: buildTextStyle(),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: size.height * 0.38),
                    padding: EdgeInsets.only(
                      top: size.height * 0.01,
                      left: kDefaultPadding,
                      right: kDefaultPadding,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Text(
                      layanan.deskripsi,
                      style: buildTextDeskripsi(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
          side: BorderSide(color: Colors.grey[300]),
        ),
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FormLayanan(
              layanan: layanan,
            ),
          ),
        ),
        child: const Text('Continue', style: TextStyle(fontSize: 20)),
        color: kPrimaryColor,
        textColor: Colors.white,
        elevation: 5,
      ),
    );
  }

  TextStyle buildHeading() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Futura',
      fontWeight: FontWeight.bold,
      fontSize: 18,
    );
  }

  TextStyle buildTextStyle() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Futura',
      fontWeight: FontWeight.bold,
      fontSize: 20,
    );
  }

  TextStyle buildTextDeskripsi() {
    return TextStyle(
      color: Colors.grey[800],
      fontFamily: 'Futura',
      fontWeight: FontWeight.bold,
      fontSize: 16,
    );
  }
}
