import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:stnkgo/screens/layanan/components/form/form_stnk.dart';
import 'package:stnkgo/screens/layanan/components/form/form_stnk5.dart';
import 'package:stnkgo/screens/layanan/components/form/form_bbn.dart';
import 'package:stnkgo/screens/layanan/components/form/form_mutasi.dart';
import 'package:stnkgo/screens/layanan/components/layanan.dart';

import 'package:stnkgo/screens/home/components/home_screen.dart';

import '../../../utils/apirequest.dart';
import '../../../components/dialogs.dart';
import '../../../enums/constants.dart';

class FormLayanan extends StatefulWidget {
  final Layanan layanan;

  const FormLayanan({Key key, this.layanan}) : super(key: key);

  @override
  _FormLayananState createState() => _FormLayananState();
}

class _FormLayananState extends State<FormLayanan> {
  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final ValueChanged _onChanged = (val) => print(val);
  final _namaController = TextEditingController();
  final _nopolController = TextEditingController();
  final _hpController = TextEditingController();
  ApiRequest api = new ApiRequest();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            (widget.layanan.layanan == '1')
                ? FormStnk(
                    kode_layanan: widget.layanan.layanan,
                    jenis: widget.layanan.jenis,
                    fbKey: _fbKey,
                    namaController: _namaController,
                    nopolController: _nopolController,
                    hpController: _hpController,
                    onChanged: _onChanged,
                  )
                : (widget.layanan.layanan == '2')
                    ? FormStnk5(
                        kode_layanan: widget.layanan.layanan,
                        jenis: widget.layanan.jenis,
                        fbKey: _fbKey,
                        namaController: _namaController,
                        nopolController: _nopolController,
                        hpController: _hpController,
                        onChanged: _onChanged,
                      )
                    : (widget.layanan.layanan == '3')
                        ? FormMutasi(
                            kode_layanan: widget.layanan.layanan,
                            jenis: widget.layanan.jenis,
                            fbKey: _fbKey,
                            namaController: _namaController,
                            nopolController: _nopolController,
                            hpController: _hpController,
                            onChanged: _onChanged,
                          )
                        : (widget.layanan.layanan == '4')
                            ? FormBbn(
                                kode_layanan: widget.layanan.layanan,
                                jenis: widget.layanan.jenis,
                                fbKey: _fbKey,
                                namaController: _namaController,
                                nopolController: _nopolController,
                                hpController: _hpController,
                                onChanged: _onChanged,
                              )
                            : Text("ERROR"),
          ],
        ),
      ),
      bottomNavigationBar: Padding(
        padding: EdgeInsets.all(8.0),
        child: RaisedButton(
          elevation: 1,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.grey[300]),
          ),
          onPressed: () {
            if (_fbKey.currentState.saveAndValidate()) {
              print(_fbKey.currentState.value);
              Dialogs.showLoadingDialog(context, _keyLoader);
              api
                  .createOrder2(
                _fbKey.currentState.value['date'].toString(),
                _fbKey.currentState.value['jenis'],
                _fbKey.currentState.value['kode_layanan'],
                _fbKey.currentState.value['jenis_kendaraan'],
                _fbKey.currentState.value['nama'],
                _fbKey.currentState.value['nopol'],
                _fbKey.currentState.value['hp'],
                _fbKey.currentState.value['cabang'],
              )
                  .then((result) {
                if (result == 'success') {
                  Navigator.of(_keyLoader.currentContext).pop();
                  return showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // Navigator.of(_keyLoader.currentContext).pop();
                      // return object of type Dialog
                      return AlertDialog(
                        title: new Text("Thank You"),
                        content: new Text(
                            "Thank's for your kindness & trust\n\nAdmin STNK GO akan segera menghubungi anda dalam waktu dekat.\n\nTekan tombol dibawah untuk konfirmasi pesanan anda."),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          new FlatButton(
                            child: new Text("Hubungi Kami"),
                            onPressed: () {
                              launchWhatsapp(
                                number: "+6285943794777",
                                nama: _fbKey.currentState.value['nama'],
                                nopol: _fbKey.currentState.value['nopol'],
                              );
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      HomeScreen(),
                                ),
                                (route) => false,
                              );
                            },
                          ),
                        ],
                      );
                    },
                  );
                } else {
                  // _showDialog();
                  SnackBar(content: Text('validation failed'));
                }
              });
            } else {
              // print(_fbKey.currentState.value);
              SnackBar(content: Text('validation failed'));
            }
          },
          color: kPrimaryColor,
          textColor: Colors.white,
          child: Text('Pesan Sekarang', style: TextStyle(fontSize: 20)),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back_arrow.svg",
            color: kPrimaryColor),
        onPressed: () => Navigator.pop(context),
      ),
      centerTitle: true,
      title: Text(
        "Order Detail".toUpperCase(),
        style: TextStyle(
          color: kPrimaryColor,
          fontFamily: 'Futura',
          fontSize: 20.0,
        ),
      ),
    );
  }

  void launchWhatsapp(
      {@required number, @required nama, @required nopol}) async {
    String name = "*$nama*".toUpperCase();
    String nop = "*$nopol*".toUpperCase();
    String message =
        "Hallo Admin STNK GO \nSaya $name,\ntelah melakukan order perpanjangan pajak di apps stnkgo dengan \nNomor Polisi: $nop \nMohon Konfirmasinya\nTerima Kasih.";
    String url = "whatsapp://send?phone=$number&text=$message";

    await canLaunch(url) ? launch(url) : print("Can't Open Whatsapp");
  }
}
