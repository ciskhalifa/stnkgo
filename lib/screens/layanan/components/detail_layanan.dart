import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:stnkgo/screens/layanan/components/layanan.dart';

import '../../../enums/constants.dart';
import 'deskripsi_layanan.dart';

class DetailLayanan extends StatelessWidget {
  final Layanan layanan;

  const DetailLayanan({Key key, this.layanan}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildAppBar(context),
      body: DeskripsiLayanan(layanan: layanan),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back_arrow.svg",
            color: kPrimaryColor),
        onPressed: () => Navigator.pop(context),
      ),
      centerTitle: true,
      title: Text(
        "Detail Layanan".toUpperCase(),
        style: TextStyle(
          color: kPrimaryColor,
          fontFamily: 'Futura',
          fontSize: 20.0,
        ),
      ),
    );
  }
}
