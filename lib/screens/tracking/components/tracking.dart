import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stnkgo/components/timeline2.dart';
import 'package:stnkgo/enums/model.dart';
import '../../../utils/apirequest.dart';
import '../../../enums/constants.dart';

class Tracking extends StatelessWidget {
  const Tracking({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildAppBar(context),
      body: SingleChildScrollView(
        child: FutureBuilder<Result>(
          future: ApiRequest().getDataTracking(title),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<Status> tracking = snapshot.data.status.toList();
              List<TimelineModel> list = [
                for (var i = 0; i < tracking.length; i++)
                  TimelineModel(
                    id: '$i',
                    title: tracking[i].title,
                    description: tracking[i].pesan,
                    date: tracking[i].date,
                    lineColor: Colors.green,
                    descriptionColor: Colors.black,
                    titleColor: Colors.green,
                  ),
              ];
              return ColumnData(
                size: size,
                title: title,
                list: list,
                invoice: snapshot.data.invoice,
                jenis: snapshot.data.jenis,
                kendaraan: snapshot.data.kendaraan,
                nopol: snapshot.data.nopol,
                resi: snapshot.data.noResi,
                status_stnk: snapshot.data.statusStnk,
                tanggal: snapshot.data.tanggal,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(kPrimaryColor),
                ),
              );
            }
          },
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: SvgPicture.asset("assets/icons/back_arrow.svg",
            color: kPrimaryColor),
        onPressed: () => Navigator.pop(context),
      ),
      centerTitle: true,
      title: Text(
        title.toUpperCase(),
        style: TextStyle(
          color: kPrimaryColor,
          fontFamily: 'Futura',
          fontSize: 20.0,
        ),
      ),
    );
  }
}

class ColumnData extends StatelessWidget {
  const ColumnData({
    Key key,
    @required this.size,
    @required this.title,
    @required this.invoice,
    @required this.jenis,
    @required this.kendaraan,
    @required this.nopol,
    @required this.tanggal,
    // ignore: non_constant_identifier_names
    @required this.status_stnk,
    @required this.resi,
    @required this.list,
  }) : super(key: key);

  final Size size;
  final String title;
  final String invoice;
  final String jenis;
  final String kendaraan;
  final String nopol;
  final String tanggal;
  // ignore: non_constant_identifier_names
  final String status_stnk;
  final String resi;

  final List<TimelineModel> list;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: size.height * 0.2,
          child: Center(
            child: SvgPicture.asset(
              "assets/images/logo2.svg",
              height: 90.0,
              width: 100.0,
              allowDrawingOutsideViewBox: false,
              alignment: Alignment.center,
            ),
          ),
          decoration: BoxDecoration(
            color: kPrimaryColor,
          ),
        ),
        Container(
          height: 50,
          width: size.width,
          padding: EdgeInsets.fromLTRB(20, 15, 0, 0),
          margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
          decoration: BoxDecoration(
            color: kPrimaryColor,
          ),
          child: Text(
            "Detail Pesanan",
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Futura',
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          width: size.width,
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Card(
            child: Column(
              children: <Widget>[
                buildPaddingDetail("No. Invoice", "$invoice"),
                buildPaddingDetail("Jenis Layanan", "$jenis"),
                buildPaddingDetail("Kendaraan", "$kendaraan"),
                buildPaddingDetail("No. Polisi", "$nopol"),
                buildPaddingDetail("Tanggal Transaksi", "$tanggal"),
                buildPaddingDetail("Status", "$status_stnk"),
                buildPaddingDetail("No. Resi", "$resi"),
              ],
            ),
          ),
        ),
        Container(
          height: 50,
          width: size.width,
          margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
          decoration: BoxDecoration(
            color: kPrimaryColor,
          ),
          child: Center(
            child: Text(
              "Tracking Status Pesanan",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Futura',
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        TimelineComponent(timelineList: list)
      ],
    );
  }

  Padding buildPaddingDetail(String title, String value) {
    return Padding(
      padding: EdgeInsets.only(top: 8.0, bottom: 8.0, left: 5.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              // align the text to the left instead of centered
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: buildTextStyleTitle(),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              // align the text to the left instead of centered
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  '$value',
                  style: buildTextStyleSub(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  TextStyle buildTextStyleSub() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Futura',
      fontSize: 13,
      fontWeight: FontWeight.bold,
    );
  }

  TextStyle buildTextStyleTitle() {
    return TextStyle(
      color: Colors.black,
      fontFamily: 'Futura',
      fontSize: 13,
      fontWeight: FontWeight.bold,
    );
  }
}
