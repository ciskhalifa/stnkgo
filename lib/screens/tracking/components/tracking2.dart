import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stnkgo/components/timeline2.dart';
// import 'package:stnkgo/enums/model.dart';
import '../../../utils/apirequest.dart';
import '../../../enums/constants.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

String apiurl = "https://stnkgo.com/api";
Future<Result> getDataTracking(String resi) async {
  String endpoint = "?resi=${resi}";
  var response = await http.get(apiurl + endpoint);
  if (response.statusCode == 200) {
    return Result.fromJson(json.decode(response.body));
  } else {
    throw "Can't get data.";
  }
}

Future<Album> fetchAlbum() async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/albums/1');

  // Appropriate action depending upon the
  // server response
  if (response.statusCode == 200) {
    return Album.fromJson(json.decode(response.body));
  } else {
    throw Exception('Failed to load album');
  }
}

class Album {
  final int userId;
  final int id;
  final String title;

  Album({this.userId, this.id, this.title});

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }
}

class Result {
  final String message;

  Result({this.message});

  factory Result.fromJson(Map<String, dynamic> json) {
    return Result(message: json['message']);
  }
}

class Tracking2 extends StatelessWidget {
  const Tracking2({Key key, this.title}) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    Future<Album> futureAlbum;
    Future<Result> getTrack;
    futureAlbum = fetchAlbum();
    getTrack = getDataTracking(title);
    return MaterialApp(
      title: 'Fetching Data',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('GeeksForGeeks'),
        ),
        body: Center(
          child: FutureBuilder<Result>(
            future: getTrack,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Text(snapshot.data.message);
              } else if (snapshot.hasError) {
                return Text("${snapshot.error}");
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
