import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stnkgo/components/timeline2.dart';
import 'package:stnkgo/enums/constants.dart';

class Body extends StatelessWidget {
  final String title;
 
  final List<TimelineModel> list = [
    TimelineModel(
        id: "1",
        description: "World Best Website",
        lineColor: Colors.yellow,
        descriptionColor: Colors.green,
        titleColor: Colors.green,
        title: "Flutter"),
    TimelineModel(
        id: "2",
        lineColor: Colors.red,
        description: "Flutter Interview Question \nTop 10 display",
        title: "Flutter Interview Question"),
    TimelineModel(
        id: "3",
        description: "Every pattern avialble in \nwww.fluttertutorial.in",
        lineColor: Colors.black,
        title: "Flutter")
  ];

  Body({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // ApiRequest().getDataTracking(title).then((value) => print(value.toJson()));
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            height: size.height * 0.2,
            child: Center(
              child: SvgPicture.asset(
                "assets/images/logo2.svg",
                height: 90.0,
                width: 100.0,
                allowDrawingOutsideViewBox: false,
                alignment: Alignment.center,
              ),
            ),
            decoration: BoxDecoration(
              color: kPrimaryColor,
            ),
          ),
          Container(
            height: 50,
            width: size.width,
            padding: EdgeInsets.fromLTRB(20, 15, 0, 0),
            margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
            decoration: BoxDecoration(
              color: kPrimaryColor,
            ),
            child: Text(
              "$title",
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Futura',
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            width: size.width,
            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
            margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "No. Invoice",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Futura',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "TEST",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Futura',
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Jenis Layanan : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Kendaraan : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "No. Polisi : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Tanggal Transaksi : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Status : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "No. Resi : ",
                    style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Futura',
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 50,
            width: size.width,
            margin: EdgeInsets.symmetric(vertical: kDefaultPadding - 15),
            decoration: BoxDecoration(
              color: kPrimaryColor,
            ),
            child: Center(
              child: Text(
                "Tracking Status Pesanan",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Futura',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          TimelineComponent(timelineList: list)
        ],
      ),
    );
  }
}
