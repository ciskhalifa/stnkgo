//import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:stnkgo/components/splashscreen.dart';
import 'package:stnkgo/enums/constants.dart';

import 'screens/home/components/home_screen.dart';

void main() {
  runApp(MyApp());
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
}

// void main() => runApp(
//       DevicePreview(
//         builder: (context) => MyApp(),
//       ),
//     );

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'STNK GO',
      theme: ThemeData(
        scaffoldBackgroundColor: kBackgroundColor,
        primaryColor: kPrimaryColor,
        textTheme: Theme.of(context).textTheme.apply(bodyColor: kTextColor),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreenPage(),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => new HomeScreen(),
      },
    );
  }
}
