import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF0C9869);
const kTextColor = Color(0xFF3C4046);
const kTextBlack = Color(0xFF000000);
const kTitleText1 = Color(0xFFF9F8FD);
const kTitleText2 = Color(0xFFFDD835);
const kBackgroundColor = Color(0xFFF9F8FD);

const double kDefaultPadding = 20.0;
