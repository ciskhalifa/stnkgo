import 'package:meta/meta.dart';
import 'dart:convert';

// GET TRACKING
Result resultFromJson(String str) => Result.fromJson(json.decode(str));

String resultToJson(Result data) => json.encode(data.toJson());

class Result {
  Result({
    @required this.message,
    @required this.invoice,
    @required this.jenis,
    @required this.kendaraan,
    @required this.nopol,
    @required this.tanggal,
    @required this.statusStnk,
    @required this.noResi,
    @required this.status,
  });

  final String message;
  final String invoice;
  final String jenis;
  final String kendaraan;
  final String nopol;
  final String tanggal;
  final String statusStnk;
  final String noResi;
  final List<Status> status;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        message: json["message"],
        invoice: json["invoice"],
        jenis: json["jenis"],
        kendaraan: json["kendaraan"],
        nopol: json["nopol"],
        tanggal: json["tanggal"],
        statusStnk: json["status_stnk"],
        noResi: json["no_resi"],
        status:
            List<Status>.from(json["status"].map((x) => Status.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "invoice": invoice,
        "jenis": jenis,
        "kendaraan": kendaraan,
        "nopol": nopol,
        "tanggal": tanggal,
        "status_stnk": statusStnk,
        "no_resi": noResi,
        "status": List<dynamic>.from(status.map((x) => x.toJson())),
      };
}

class Status {
  Status({
    @required this.title,
    @required this.pesan,
    @required this.date,
  });

  final String title;
  final String pesan;
  final String date;

  factory Status.fromJson(Map<String, dynamic> json) => Status(
        title: json["title"],
        pesan: json["pesan"],
        date: json["date"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "pesan": pesan,
        "date": date,
      };
}
// END TRACKING

// GET CABANG
ResultCabang resultCabangFromJson(String str) =>
    ResultCabang.fromJson(json.decode(str));

String resultCabangToJson(ResultCabang data) => json.encode(data.toJson());

class ResultCabang {
  ResultCabang({
    @required this.message,
    @required this.result,
  });

  final String message;
  final List<Cabang> result;

  factory ResultCabang.fromJson(Map<String, dynamic> json) => ResultCabang(
        message: json["message"],
        result:
            List<Cabang>.from(json["result"].map((x) => Cabang.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "cabang": List<dynamic>.from(result.map((x) => x.toJson())),
      };
}

class Cabang {
  Cabang({
    @required this.kode,
    @required this.cabang,
  });

  final String kode;
  final String cabang;

  factory Cabang.fromJson(Map<String, dynamic> json) => Cabang(
        kode: json["kode"],
        cabang: json["cabang"],
      );

  Map<String, dynamic> toJson() => {
        "kode": kode,
        "cabang": cabang,
      };
}
