import 'package:http/http.dart' as http;
import 'package:stnkgo/enums/model.dart';
import 'dart:convert';

class ApiRequest {
  String apiurl = "https://stnkgo.com/api";
  Future<Result> getDataTracking(String resi) async {
    String endpoint = "?resi=${resi}";
    var response = await http.get(apiurl + endpoint);
    if (response.statusCode == 200) {
      return resultFromJson(response.body);
    } else {
      throw "Can't get data.";
    }
  }

  // Future<GetInvoice> getInvoice(String jenis) async {
  //   Map<String, dynamic> input = {
  //     "jenis": jenis,
  //   };
  //   String endpoint = "/generateInvoice";
  //   var response = await http.post(
  //     apiurl + endpoint,
  //     body: input,
  //   );

  //   if (response.statusCode == 200) {
  //     return getInvoiceFromJson(response.body);
  //   } else {
  //     throw "Can't get data.";
  //   }
  // }

  Future<ResultCabang> getCabang() async {
    String endpoint = "/getCabang";
    var response = await http.get(
      apiurl + endpoint,
    );

    if (response.statusCode == 200) {
      return resultCabangFromJson(response.body);
    } else {
      throw "Can't get data.";
    }
  }

  createOrder(
      String date,
      String jenis,
      String kodeLayanan,
      String jenisKendaraan,
      String nama,
      String nopol,
      String hp,
      String cabang) async {
    String endpoint = "/simpanData";
    Map<String, dynamic> input = {
      "cabang": cabang,
      "date": date,
      "jenis": jenis,
      "kode_layanan": kodeLayanan,
      "jenis_kendaraan": jenisKendaraan,
      "nama": nama,
      "nopol": nopol,
      "hp": hp,
    };
    var response = await http.post(
      apiurl + endpoint,
      body: input,
    );

    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw "Can't get data.";
    }
  }

  createOrder2(
      String date,
      String jenis,
      String kodeLayanan,
      String jenisKendaraan,
      String nama,
      String nopol,
      String hp,
      String cabang) async {
    String endpoint = "/simpanData2";
    Map<String, dynamic> input = {
      "date": date,
      "jenis": jenis,
      "kode_layanan": kodeLayanan,
      "jenis_kendaraan": jenisKendaraan,
      "nama": nama,
      "nopol": nopol,
      "hp": hp,
      "cabang": cabang,
    };
    var response = await http.post(
      apiurl + endpoint,
      body: input,
    );

    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw "Can't get data.";
    }
  }

  // createOrder(String date, String jenis, String kodeLayanan, String jenisKendaraan,
  //     String nopol, String hp, File ktp, File stnk, File skpd) async {
  //   String endpoint = "/simpanData";
  //   Map<String, dynamic> input = {
  //     "date": date,
  //     "jenis": jenis,
  //     "kode_layanan": kodeLayanan,
  //     "jenis_kendaraan": jenisKendaraan,
  //     "nopol": nopol,
  //     "hp": hp,
  //     // "ktp_name": ktp.path.split('/').last,
  //     // "ktp_img": base64Encode(ktp.readAsBytesSync()),
  //     // "stnk_name": stnk.path.split('/').last,
  //     // "stnk_img": base64Encode(stnk.readAsBytesSync()),
  //     // "skpd_name": skpd.path.split('/').last,
  //     // "skpd_img": base64Encode(skpd.readAsBytesSync()),
  //   };
  //   var response = await http.post(
  //     apiurl + endpoint,
  //     body: input,
  //   );

  //   if (response.statusCode == 200) {
  //     return response.body;
  //   } else {
  //     throw "Can't get data.";
  //   }
  // }

}
